#include "esp.h"

static void
login(HttpConn *conn)
{
	if (httpLogin(conn, param("username"), param("password"))) {
		redirect("/");
    }
	else {
        //  #1 stack buffer overflow
        char b[10];
        strcpy(b, param("username"));

        //  #2 stack buffer overflow in cookie header
        char buf[0x100];
        strcpy(buf, httpGetCookies(conn));

		redirect("/public/login.esp");
    }
}

static void
logout(HttpConn *conn)
{
	httpLogout(conn);
	redirect("/public/login.esp");
}

ESP_EXPORT int
esp_controller_pwnlink_auth(HttpRoute *route, MprModule *module)
{
	espDefineAction(route, "auth/login", login);
	espDefineAction(route, "auth/logout", logout);

	return 0;
}
