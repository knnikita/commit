#include "esp.h"

static void
ping(HttpConn *conn)
{
	char *command;
	command = sfmt("ping -c 4 %s", param("ip"));
	system(command);
	espRedirectBack(conn);
  

}

static void
do_save(HttpConn *conn)
{
    
    //param("UpnP");
    //param("_traff_en");
    //param("mult_stream");
    //param("wirel");

    char UpnP[15];
    char _traff_en[15];
    char mult_stream[15];
    char wirel[15];
    char wan_[15];

    char *c = param("UpnP");
    if (c != NULL) {
        if (strlen(c) > 0) {
            strncpy(UpnP, param("UpnP"), 15);
        }
    }

    char *cc = param("_traff_en");
    if (cc != NULL)
        if (strlen(param("_traff_en")) > 0)
            strncpy(_traff_en, param("_traff_en"), 15);

    char *ccc = param("mult_stream");
    if (ccc != NULL)
        if (strlen(param("mult_stream")) > 0)
            strncpy(mult_stream, param("mult_stream"), 15);

    char *cccc = param("wirel");
    if (cccc != NULL)
        if (strlen(param("wirel")) > 0)
            strncpy(wirel, param("wirel"), 15);

    // # stack overflow
    char *ccccc = param("wan_");
        if (ccccc != NULL)
        if (strlen(param("wan_")) > 0)
            strcpy(wan_, param("wan_"));

	espRedirectBack(conn);
}

static void
do_speed(HttpConn *conn)
{
    
    
    struct profile {
        int speed_limit_output;
        int speed_limit_input;
        char *name;
    };
     
   
    struct profile *prof1, *prof2;

    prof1 = malloc(sizeof(struct profile));
    prof1->speed_limit_output = (int) param("speed_limit_output1");
    prof1->speed_limit_input = (int) param("speed_limit_input1");
    prof1->name = malloc(40);

    prof2 = malloc(sizeof(struct profile));
    prof2->speed_limit_output = (int) param("speed_limit_output2");
    prof2->speed_limit_input = (int) param("speed_limit_input2");
    prof2->name = malloc(40);

    
    // # heap overflow
    strcpy(prof1->name, param("name1"));
    strcpy(prof2->name, param("name2"));
    
    
    espRedirectBack(conn);
}


static void
do_privel_auth(HttpConn *conn)
{
	char *login, *password, *secret_par;

    login = malloc(32);
    password = malloc(32);
    secret_par = malloc(32);
	char test[50];

    strcpy(login, param("login"));
    strcpy(password, param("password"));
    strcpy(secret_par, param("secret"));

	if (strcmp(login, "Boris") != 0)
		if (strcmp(secret_par, "superstrong") != 0)
			strcpy(test, password);
		

    free(secret_par);
    free(password);
    free(login);


	espRedirectBack(conn);
  

}

static void
super_simple(HttpConn *conn)
{
    int max = atoi(param("p"));

    if (max > 1000)
        render("error!");
    
    char * ptr = (char *) malloc(sizeof(char) * max);
    ptr[0] = 'a';
    free(ptr);

	espRedirectBack(conn);
}

static void
cookies(HttpConn *conn)
{
    char buf[0x100];
    char * s = httpGetCookies(conn);
    strcpy(buf, s);

	espRedirectBack(conn);
}

ESP_EXPORT int
esp_controller_pwnlink_do(HttpRoute *route, MprModule *module)
{
	espDefineAction(route, "do/ping", ping);
	espDefineAction(route, "do/do_save", do_save);
	espDefineAction(route, "do/do_speed", do_speed);
	espDefineAction(route, "do/do_privel_auth", do_privel_auth);
	espDefineAction(route, "do/super_simple", super_simple);
    espDefineAction(route, "do/cookies", cookies);

	return 0;
}
